FROM node:12

RUN mkdir -p /home/node/frontend && chown -R node:node /home/node/frontend

WORKDIR /home/node/frontend

COPY package*.json *yarn* ./

RUN chown -R node:node .

USER node

RUN yarn

COPY --chown=node:node . .

ENV PATH="${PATH}:/home/node/frontend/node_modules/.bin"

EXPOSE 3000

ENV CI=true

CMD [ "yarn", "run", "start" ]
