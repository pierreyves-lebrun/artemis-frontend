import React from 'react'
import { useAuthentication } from 'helpers/hooks/useAuthentication'

export function App() {
  const [
    currentUser,
    setCurrentUser,
    authenticationStateIsUnknown
  ] = useAuthentication()

  // Don't render anything until we know user authentication state
  if (authenticationStateIsUnknown) {
    return null
  }

  return <h1>Hello world</h1>
}
