import { ApolloClient, InMemoryCache } from '@apollo/client'

const backendHost = process.env.REACT_APP_BACKEND_HOST
const backendPort = process.env.REACT_APP_BACKEND_PORT

export const client = new ApolloClient({
  uri: `http://${backendHost}:${backendPort}`,
  credentials: 'include',
  cache: new InMemoryCache()
})
