import { useState } from 'react'
import { User } from 'types'
import { useMeQuery } from 'generated/graphql'

/**
 * Custom hook that handles current user authentication state
 */
export const useAuthentication = (): [User | undefined, Function, boolean] => {
  const { data, loading, error } = useMeQuery()
  const [currentUser, setCurrentUser] = useState<User | undefined>(undefined)

  if (error) {
    return [currentUser || undefined, setCurrentUser, loading]
  }

  if (data && data.me) {
    const userRetrievedFromCookie = data.me
    return [userRetrievedFromCookie, setCurrentUser, loading]
  }

  return [currentUser, setCurrentUser, loading]
}
