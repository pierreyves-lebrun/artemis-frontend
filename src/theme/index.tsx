import blue from '@material-ui/core/colors/blue'
import pink from '@material-ui/core/colors/pink'
import { createMuiTheme } from '@material-ui/core/styles'

// A custom theme for this app
export const theme = createMuiTheme({
  props: {
    // Name of the component ⚛️
    /** MuiButtonBase: {
      * // The properties to apply
      * disableRipple: true, // No more ripple, on the whole application 💣!
    },*
    */
  },
  palette: {
    primary: blue,
    secondary: pink
  }
})
